<!DOCTYPE html>
<html>

<head>
    <title>หลักสูตร</title>
    <?php include 'include/inc-header.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-head.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li class="active">หลักสูตรทั้งหมด</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="d-flex row ">

                <div class="col-lg-2 col-md-3">
                    <aside class="sidebar">
                        <table class="table table-bordered">
                            <thead>
                                <tr style="background-color: #F7C011">
                                    <th class="text-font-allcourse" scope="col"><img src="./img/1-main/menu1.png" alt=""> &nbsp;หมวดหมู่หลักสูตร</th>
                                </tr>
                            </thead>
                            <tbody class="toggle toggle-minimal toggle-primary ">
                                <tr>
                                    <th scope="row" data-plugin-toggle data-plugin-options="{ 'isAccordion': true }">
                                        <section class="toggle active">
                                            <a class="toggle-title text-4 px-3">ประเภท 1</a>
                                            <div class="toggle-content mx-4 ">
                                                <p><a class="text-dark" href="#">หลักสูตร 1</a></p>
                                                <p><a class="text-dark" href="#">หลักสูตร 2</a></p>
                                                <p><a class="text-dark" href="#">หลักสูตร 3</a></p>
                                            </div>
                                        </section>

                                    </th>

                                </tr>
                                <tr>
                                    <th scope="row" data-plugin-toggle data-plugin-options="{ 'isAccordion': true }">
                                        <section class="toggle">
                                            <a class="toggle-title text-4 px-3">ประเภท 2</a>
                                            <div class="toggle-content mx-4 ">
                                                <p><a class="text-dark" href="#">หลักสูตร 1</a></p>
                                                <p><a class="text-dark" href="#">หลักสูตร 2</a></p>
                                                <p><a class="text-dark" href="#">หลักสูตร 3</a></p>
                                            </div>
                                        </section>

                                    </th>
                                </tr>
                                <tr>
                                    <th scope="row" data-plugin-toggle data-plugin-options="{ 'isAccordion': true }">

                                        <section class="toggle">
                                            <a class="toggle-title text-4 px-3">ประเภท 3</a>
                                            <div class="toggle-content mx-4 ">
                                                <p><a class="text-dark" href="#">หลักสูตร 1</a></p>
                                                <p><a class="text-dark" href="#">หลักสูตร 2</a></p>
                                                <p><a class="text-dark" href="#">หลักสูตร 3</a></p>
                                            </div>
                                        </section>

                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </aside>
                </div>

                <div class="col-lg-10 col-md-9 section-allcourse">

                    <div class="row g-5">

                        <div class="col-lg-4 col-md-6">
                            <div class="card card-course">
                                <a href="#">
                                    <img class="card-img-top" src="img/1-main/Course-banner1.png">
                                </a>
                                <div class="card-body">
                                    <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                    <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                    <hr class="mb-2">
                                    <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                    <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 ">
                            <div class="card card-course">
                                <a href="#">
                                    <img class="card-img-top" src="img/1-main/Course-banner2.png">
                                </a>
                                <div class="card-body">
                                    <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                    <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                    <hr class="mb-2">
                                    <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                    <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 ">
                            <div class="card card-course">
                                <a href="#">
                                    <img class="card-img-top" src="img/1-main/Course-banner3.png">
                                </a>
                                <div class="card-body">
                                    <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                    <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                    <hr class="mb-2">
                                    <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                    <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 ">
                            <div class="card card-course">
                                <a href="#">
                                    <img class="card-img-top" src="img/1-main/Course-banner4.png">
                                </a>
                                <div class="card-body">
                                    <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                    <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                    <hr class="mb-2">
                                    <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                    <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 ">
                            <div class="card card-course">
                                <a href="#">
                                    <img class="card-img-top" src="img/1-main/Course-banner5.png">
                                </a>
                                <div class="card-body">
                                    <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                    <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                    <hr class="mb-2">
                                    <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                    <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 ">
                            <div class="card card-course">
                                <a href="#">
                                    <img class="card-img-top" src="img/1-main/Course-banner6.png">
                                </a>
                                <div class="card-body">
                                    <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                    <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                    <hr class="mb-2">
                                    <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                    <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 ">
                            <div class="card card-course">
                                <a href="#">
                                    <img class="card-img-top" src="img/1-main/Course-banner7.png">
                                </a>
                                <div class="card-body">
                                    <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                    <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                    <hr class="mb-2">
                                    <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                    <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6">
                            <div class="card card-course">
                                <a href="#">
                                    <img class="card-img-top" src="img/1-main/Course-banner8.png">
                                </a>
                                <div class="card-body">
                                    <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                    <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                    <hr class="mb-2">
                                    <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                    <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>


            </div>
        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>