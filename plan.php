<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .table-plan-container {
            overflow-x: auto;
        }

        .table-plan {
            border-collapse: collapse;
            display: grid;
            width: 100%;
            grid-template-columns: 100px repeat(3,minmax(70px, 2fr));
            grid-template-rows: 60px;
        }

        .cell {
            border: 1px solid #dddddd;
            padding: 15px 5px;
            text-align: center;
        }

        .cell:nth-of-type(4n + 1) {
            grid-column: 1/1;
            text-align: center;
            padding: 15px 20px;
        }

        .cell:nth-of-type(4n + 2) {
            grid-column: 2/2;
        }

        .cell:nth-of-type(4n + 3) {
            grid-column: 3/3;
        }

        .cell:nth-of-type(4n + 4) {
            grid-column: 4/4;
        }


        .th {
            font-weight: bold;
            border-top: 2px solid #dddddd;
            border-bottom: 2px solid #dddddd;
            background-color: #FBFBFB;
        }

        .event {
            padding: 0 10px;
            position: relative;
            align-self: center;
            color: #ffffff;
            height: 100%;

        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main ">
            <div class="container-fluid">
                <section class="page-header page-header-modern pb-3 mb-0 ">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 align-self-center ">
                                <ul class="breadcrumb d-block">
                                    <li><a href="./">หน้าแรก</a></li>
                                    <li class="active">แผนการเรียน</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container-fulid content">
                    <div class="py-2 ">
                        <h4 class="mx-5">>> ปี 2564<span> : ระยะเวลาที่สามารถเรียนได้แต่ละหลักสูตร</span></h4>
                        <div class="mx-5 my-2 ">
                            <div class="table-plan-container">
                                <div id="table-plan" class="table-plan">
                                    <div class='cell th'>ลำดับ</div>
                                    <div class='cell th'>ชื่อหลักสูตร</div>
                                    <div class='cell th'>ช่วงเวลาอบรม (ภายใน 15 วัน )</div>
                                    <div class='cell th'>พิมพ์ใบประกาศนียบัตร (ภายใน 45 วัน)</div>

                                    <div class='cell' style='grid-row:2;'>1</div>
                                    <div class='cell' style='grid-row:2;text-align:start;'>
                                        การประกันอัคคีภัยและประกันความเสี่ยงภัยทุกชนิด</div>
                                    <div class='cell' style='grid-row:2;'></div>
                                    <div class='cell' style='grid-row:2;'></div>

                                    <div class='cell' style='grid-row:3;'>2</div>
                                    <div class='cell' style='grid-row:3; text-align:start;'>การประกันสุขภาพและประกันโรคร้ายแรง
                                    </div>
                                    <div class='cell' style='grid-row:3;'>1 ม.ค. - 15 ม.ค. 64</div>
                                    <div class='cell' style='grid-row:3;'>1 ม.ค. - 15 ม.ค. 64</div>
                                    <div class='cell' style='grid-row:4;'>3</div>
                                    <div class='cell' style='grid-row:4;text-align:start;'>DISC & TA
                                        เพื่อการสื่อสารและการพัฒนาตนเองอย่างมีประสิทธิภาพ 1</div>
                                    <div class='cell' style='grid-row:4;'></div>
                                    <div class='cell' style='grid-row:4;'></div>
                                    <div class='cell' style='grid-row:5;'>4</div>
                                    <div class='cell' style='grid-row:5; text-align:start;'>การกำกับดูแลพฤติกรรมทางการตลาด</div>
                                    <div class='cell' style='grid-row:5;'>1 ม.ค. - 15 ม.ค. 64</div>
                                    <div class='cell' style='grid-row:5;'>1 ม.ค. - 15 ม.ค. 64</div>

                                    <section class='event text-center' style='grid-row:2; grid-column: 3 / span 1; background-color:#3A8DDD;'>
                                        <div class="mt-3">
                                            1 ม.ค. - 15 ม.ค. 64
                                        </div>
                                    </section>

                                    <section class='event text-center' style='grid-row:2; grid-column: 4 / span 1; background-color:#3A8DDD;'>
                                        <div class="mt-3">
                                            1 ม.ค. - 15 ม.ค. 64
                                        </div>
                                    </section>

                                    <section class='event text-center text-dark' style='grid-row:4; grid-column: 3 / span 1; background-color:#FFD400;'>
                                        <div class="mt-3">
                                            1 ม.ค. - 15 ม.ค. 64
                                        </div>
                                    </section>

                                    <section class='event text-center text-dark' style='grid-row:4; grid-column: 4 / span 1; background-color:#FFD400;'>
                                        <div class="mt-3">
                                            1 ม.ค. - 15 ม.ค. 64
                                        </div>
                                    </section>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mx-5 ">
                                    <div class="box-plan-status-notyet mt-1">ยังไม่ได้ดำเนินการ</div>
                                    <div class="box-plan-status-doing mt-1">กำลังดำเนินการ</div>
                                    <div class="box-plan-status-finish mt-1">ดำเนินการแล้ว</div>
                                    <div class="box-plan-status-timeout mt-1">หมดเวลา</div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


            <div style="margin-top: 10rem ">
                <?php include 'include/inc-footermain.php'; ?>
            </div>

        </div>
        <?php include 'include/inc-script.php'; ?>
    </div>
</body>

</html>