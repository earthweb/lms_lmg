<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>

    <style>

    </style>

</head>

<body data-plugin-page-transition>

    <div class="body">
        <div role="main" class="main">


            <div class="container-fulid content">
                <div class="row  ">

                    <div id="branding" class="col-md-4 d-none d-md-block"></div>
                    <div class="col col-md-4  ">
                        <div class="login-section ">
                            <div>
                                <img src="img/1-main/icon-p2.png" alt="">
                            </div>

                            <h3 class="pt-5 text-center">เข้าสู่ระบบ</h3>
                            <form action="/" id="frmSignIn" method="post" class="needs-validation">
                                <div class="row align-items-center g-3">
                                    <div class="form-group col-auto">
                                        <img src=".\img\1-main\username-icon.png">
                                    </div>
                                    <div class="form-group col">
                                        <input type="text" value="" class="form-control form-control-lg"
                                            placeholder="ชื่อผู้ใช้งาน" required>
                                    </div>
                                </div>
                                <div class="row align-items-center g-3">
                                    <div class="form-group col-auto">
                                        <img src=".\img\1-main\password-icon.png">
                                    </div>
                                    <div class="form-group col">
                                        <input type="password" value="" class="form-control form-control-lg"
                                            placeholder="รหัสผ่าน" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col ">
                                        <a class="text-decoration-none text-color-dark text-color-hover-primary text-3 float-end"
                                            href="forgotpassword.php">ลืมรหัสผ่าน?</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col">
                                        <button type="submit" class="btn btn-main w-100 text-4 py-2 my-4"
                                            data-loading-text="Loading...">เข้าสู่ระบบ</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Footer โดนทับเลยต้องเขียนแยกครับ -->
        <footer class="border border-end-0 border-start-0 border-bottom-0 border-color-light-3 mt-5">
            <div class="bg-footer" style="z-index:1">
                <img src="img/1-main/main-bg-3.png">
            </div>
            <div class="copyright bg-main" style="height: auto">
                <div class="container text-center py-2">
                    <p class="mb-0 text-2 text-light"><code class="text-light">&copy;</code> 2021 by LMG Insurance Public Company Limited All right reserved.</p>
                </div>
            </div>
        </footer>
    </div>

    </div>
    </div>
   
    <?php include 'include/inc-script.php'; ?>
</body>

</html>