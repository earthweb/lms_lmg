<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .learn-navbar {
            border-bottom: 1px solid #DADADA;
            background-color: #ffffff;
            z-index: 10;
            position: sticky;
            top: 0;
        }

        #note {
            border-left: 1px solid #DADADA;
            height: 100vh;
            background-color: #ffffff;
            z-index: 10;
            position: fixed;
            top: 57px;
            right: 0;
        }

        .note-sm {
            width: 70vw;
            left: 100vw;
        }

        .transition {
            transition: 0.5s;
        }

        .open-note-sm {
            transform: translatex(-70vw);
        }

        .close-section {
            transform: translatex(100vw);
        }
    </style>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body data-plugin-page-transition>

    <div class="body">

        <div role="main" class="main">

            <div class="learn-navbar">
                <div class="container-fluid">
                    <div class="row align-items-center justify-content-between py-2 ">
                        <div class="col-auto">
                            <a href="./coursedetail.php"><button class="btn btn-light text-dark">กลับไปหน้าหลักสูตร</button></a>
                        </div>
                        <div class="col-auto">
                            <div class="btn-group" role="group" aria-label="Basic checkbox toggle button group">
                                <input type="checkbox" class="btn-check" id="btncheck" autocomplete="off" onclick="clickfuns();">
                                <label class="btn btn-warning" style="display: flex;" for="btncheck">
                                <i class="material-icons">developer_board</i><span class="d-none d-md-inline" style="margin-top: 3px;"> &nbsp; สไลด์เนื้อหาวิดีโอ</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row p-0 g-0 flex-nowrap">

                <div id="video-main" class="col-12 transition p-5 mb-4">
                    <div class="row flex-lg-nowrap" style="--bs-gutter-x: 0rem !important;">
                        <div id="video-container" class="col-12 transition mb-4">
                            <div class="ratio ratio-4x3">
                                <video id="video" width="100%" height="100%" muted loop preload="metadata" poster="./video/presentation.jpg">
                                    <source src="./video/movie.ogv" type="video/ogv">
                                    <source src="./video/movie.mp4" type="video/mp4">
                                </video>
                                <a href="#" class="position-absolute top-50pct left-50pct transform3dxy-n50 bg-light rounded-circle d-flex align-items-center justify-content-center text-decoration-none bg-color-hover-primary text-color-hover-light play-button-lg pulseAnim pulseAnimAnimated" data-trigger-play-video="#video" data-trigger-play-video-remove="yes">
                                    <i class="fas fa-play text-5"></i>
                                </a>
                            </div>
                        </div>

                        <div id="slide" class="transition close-section d-none d-lg-inline">
                            <figure>
                                <img alt="" class="img-fluid" src="https://via.placeholder.com/600x451">
                            </figure>
                        </div>
                    </div>
                    <div class="slide">
                        <p>คำถาม</p>
                    </div>
                    <textarea class="form-control" rows="3" placeholder="พิมพ์ข้อความ"></textarea>
                    <button type="submit" class="btn btn-warning mt-2 float-end" style="width: 80px;">ส่ง</button>
                </div>

                <div id="note" class="transition close-section" style="overflow-y: scroll;">
                    <div class="row m-0 border-bottom p-3">
                        <img alt="" class="img-fluid rounded" src="img/1-main/camera-verify.png">
                    </div>
                    <div class="row m-0 border-bottom p-3">
                        <form action="#">
                            <h6>จดบันทึก</h6>
                            <textarea class="form-control" placeholder="พิมพ์ข้อความ" rows="3"></textarea>
                            <input type="submit" value="บันทึก" class="btn btn-warning text-dark float-end px-3 mt-2">
                        </form>
                        <div class="row m-0 p-0 slide">
                            <div class="col-12">
                                <h6>รายการบันทึกของคุณ</h6>
                                <p class="mt-1"><span> 01:20 </span> &nbsp; ข้อความ1</p>
                                <p><span> 03:30 </span> &nbsp; ข้อความ2</p>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="height:auto;">
                        <div class="col-7">
                            <h6 class="mt-4 mx-4">สไลด์ประกอบวิดีโอ</h6>
                        </div>
                        <div class="col-5">
                            <button class="btn btn-outline-dark float-end mt-3" style="margin-right: 20px;"><i class="fas fa-download"></i> ดาวน์โหลด</button>
                        </div>
                    </div>
                    <div class="row m-0 p-2 mb-5">
                        <img alt="" class="rounded" src="img/1-main/slide-video.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'include/inc-script.php'; ?>

    <script>
        var chkMedia = window.matchMedia("(max-width: 767px)");
        noteResponsive(chkMedia); // Call listener function at run time
        chkMedia.addListener(noteResponsive); // Attach listener function on state changes

        function noteResponsive(chkMedia) {
            var note = document.getElementById("note"),
                main = document.getElementById("video-main");
            if (chkMedia.matches) {
                note.className = "transition note-sm";
            } else {
                note.className = "transition close-section";
                main.className = "col-12 transition p-5 mb-4";
            }
        }

        function clickfuns() {
            noteAction();
            slideAction();

            function noteAction() {
                var noteButton, main, note, x;
                noteButton = document.getElementById("btncheck");
                main = document.getElementById("video-main");
                note = document.getElementById("note");
                x = window.matchMedia("(max-width: 767px)");

                if (x.matches) {
                    noteButton.checked ? note.className += " open-note-sm" : note.className = note.className.replace(" open-note-sm", "");
                } else {
                    if (noteButton.checked) {
                        main.className = main.className.replace("col-12", "col-md-8 col-lg-9");
                        note.className = note.className.replace("close-section", "col-10 col-md-4 col-lg-3");
                    } else {
                        main.className = main.className.replace("col-md-8 col-lg-9", "col-12");
                        note.className = note.className.replace("col-10 col-md-4 col-lg-3", "close-section");
                    }
                }
            }

            function slideAction() {
                var slideButton, main, note;
                slideButton = document.getElementById("btncheck");
                video = document.getElementById("video-container");
                slide = document.getElementById("slide");
                if (slideButton.checked) {
                    video.className = video.className.replace("col-12", "col-lg-6");
                    slide.className = slide.className.replace("close-section d-none d-lg-inline", "col-lg-6");
                } else {
                    video.className = video.className.replace("col-lg-6", "col-12");
                    slide.className = slide.className.replace("col-lg-6", "close-section d-none d-lg-inline");
                }
            }
        }
    </script>

</body>

</html>