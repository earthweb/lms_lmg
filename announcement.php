<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .table-announcement {
            min-width: 500px;
            margin-bottom: 0px;
        }

        .table>:not(:last-child)>:last-child>* {
            border-bottom-color: currentColor;
            padding: 10px;
        }

        .table>:not(caption)>*>* {
            padding: .5rem .5rem;
            padding-top: 0.5rem;
            padding-right: 0.5rem;
            padding-bottom: 0.5rem;
            padding-left: 2.3rem;
        }

        .table-announcement tr td:nth-child(1) {
            text-align: left;
        }

        .table-announcement tr td:nth-child(2) {
            text-align: right;
        }

        .table-announcement thead {
            border-top: 1px solid #DDDDDD;
            border-bottom: 2px solid #DDDDDD;
        }

        .table-announcement td,
        .table-announcement th {
            border-bottom: 0px solid #DDDDDD;
            vertical-align: middle;
        }

        .table-announcement .td-border-bottom td {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: middle;
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">

                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li class="active">ตารางอบมรม/ประกาศรายชื่อ</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container content">
                <div class="row justify-content-between align-items-end mb-3">
                    <div class="col-7 col-md-8 col-lg-9">
                        <h4 class="topic mb-0"> ตารางอบรม/ประกาศรายชื่อ</h4>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col table-responsive">
                        <table class="table table-announcement">
                            <thead>
                                <tr>
                                    <td><img src="./img/1-main/topic-table.png"> ตารางประจำปี</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>ตารางอบรมประจำปี 2564</td>
                                    <td>
                                        <button class="btn btn-download" type="button">ดาวน์โหลด</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-announcement">
                            <thead>
                                <tr>
                                    <td><img src="./img/1-main/topic-table.png"> ประกาศรายชื่อผู้มีสิทธิเข้ารับการอบรม</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>รายชื่อผู้มีสิทธิเข้ารับการอบรมประจำเดือน กรกฎาคม</td>
                                    <td>
                                        <button class="btn btn-download" type="button">ดาวน์โหลด</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-announcement">
                            <thead>
                                <tr>
                                    <td><img src="./img/1-main/topic-table.png"> ประกาศรายชื่อผู้ผ่านการอบรม</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="td-border-bottom">
                                    <td>รายชื่อผู้มีสิทธิเข้ารับการอบรมประจำเดือน กรกฎาคม</td>
                                    <td>
                                        <button class="btn btn-download" type="button">ดาวน์โหลด</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footermain.php'; ?>

    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>