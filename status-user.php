<!DOCTYPE html>
<html>

<head>
    <title>สถานะการเรียน</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>
        <style>
        .table-questionnaire {
            min-width: 700px;
            text-align: center;
        }

        .table-questionnaire thead tr th {
            vertical-align: middle;
        }

        .table-questionnaire tbody tr td:nth-child(2) {
            text-align: left;
            width: 300px;
        }


        .table-questionnaire td,
        .table-questionnaire th {
            width: 85px;
        }



        #suggestion {
            width: 100%;
            height: 100px;
            padding: 10px;



        }

       
        </style>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">

                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li class="active">สถานะการเรียน</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="row g-0 position-relative">

                <div class="bg-page">
                    <img src="img/1-main/main-bg-1.png">
                </div>

                <div class="sidebar col-md-3 col-lg-2">
                    <div class="container p-3 my-3 border">
                        <div class="container-fluid ">
                            <p class="my-0"><a href="./profile.php">ข้อมูลส่วนตัว</a></p>
                            <hr class="my-2">
                            <p class="my-0">สถานะการเรียน</p>
                        </div>
                    </div>
                </div>

                <div class="content col col-md-9 col-lg-10">
                    <div class="container pb-5">
                        <div class="row g-5">

                            <div class="container content">
                                <div class="toggle toggle-minimal toggle-primary toggle-default" data-plugin-toggle>


                                    <div class="row ">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-2 ">
                                            <h4 class="text-center">ค้นหา | หลักสูตร </h4>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control"
                                                placeholder="หลักสูตรที่ต้องการค้นหา">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button"
                                                class="btn btn-warning text-dark btn-block" >ค้นหา</button>
                                        </div>
                                        <div class="col-md-2">
                                        </div>
                                    </div>


                                    <section class="toggle active mt-5">
                                        <a class="toggle-title "> <span class="numberCircle pr-3">1</span> หลักสูตร
                                            ขอรับใบอนุญาตเป็นนายหน้าประกันวินาศภััย <button type="button"
                                                class="btn btn-warning text-dark float-end"><img
                                                    src="img/1-main/icon-p.png" alt="">
                                                พิมพ์ใบประกาศนียบัตร</button></a>
                                        <div class="toggle-content ">
                                            <form>
                                                <div class="col table-responsive my-4">
                                                    <table class="table table-bordered table-questionnaire">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="2">ลำดับ</th>
                                                                <th rowspan="5" class="detail">วิชา/หลักสูตร</th>
                                                                <th rowspan="3">จำนวนชั่วโมง</th>
                                                                <th rowspan="3">สถานะการอบรม</th>
                                                                <th rowspan="3">ผลการทดสอบหลังอบรม</th>
                                                                <th rowspan="3">แบบประเมินผลการอบรม</th>
                                                                <th colspan="3">ออกใบประกาศ </th>
                                                            </tr>
                                                            <tr>
                                                                <th class="mt-2"> ภายใน วันที่ 15 ก.พ. 64</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <tr>
                                                                <td>1</td>
                                                                <td>การประกันอัคคีภัย และ การประกันความเสี่ยงภัยทุกชนิด
                                                                </td>
                                                                <td> 6</td>
                                                                <td> ยังไม่ได้อบรม</td>
                                                                <td> ผ่าน</td>
                                                                <td> ยังไม่ได้ประเมิน</td>
                                                                <td> <button type="button"
                                                                        class="btn btn-warning text-dark  "><img
                                                                            src="img/1-main/icon-p.png" alt=""></button>
                                                                </td>

                                                            </tr>


                                                            <tr>
                                                                <td>2</td>
                                                                <td>การประกันสุขภาพและประกันภัยโรคร้ายแรง</td>
                                                                <td> 6</td>
                                                                <td> ยังไม่ได้อบรม</td>
                                                                <td> ผ่าน</td>
                                                                <td> ยังไม่ได้ประเมิน</td>
                                                                <td> <button type="button"
                                                                        class="btn btn-warning text-dark  "><img
                                                                            src="img/1-main/icon-p.png" alt=""></button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="7">รวมจำนวนชั่วโมงทั้งหมด 12 ชั่วโมง</td>
                                                            </tr>
                                                        </tbody>

                                                    </table>
                                                </div>


                                            </form>

                                        </div>
                                    </section>
                                    <section class="toggle ">
                                        <a class="toggle-title "> <span class="numberCircle pr-3">2</span>
                                            ชื่อหลักสูตร<button type="button"
                                                class="btn btn-warning text-dark float-end"><img
                                                    src="img/1-main/icon-p.png" alt="">
                                                พิมพ์ใบประกาศนียบัตร</button></a>
                                        <div class="toggle-content ">
                                            <div class="col table-responsive my-4">
                                                <table class="table table-bordered table-questionnaire">
                                                    <thead>
                                                        <tr>
                                                            <th rowspan="2">ลำดับ</th>
                                                            <th rowspan="5" class="detail">วิชา/หลักสูตร</th>
                                                            <th rowspan="3">จำนวนชั่วโมง</th>
                                                            <th rowspan="3">สถานะการอบรม</th>
                                                            <th rowspan="3">ผลการทดสอบหลังอบรม</th>
                                                            <th rowspan="3">แบบประเมินผลการอบรม</th>
                                                            <th colspan="3">ออกใบประกาศ </th>
                                                        </tr>
                                                        <tr>
                                                            <th class="mt-2"> ภายใน วันที่ 15 ก.พ. 64</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>
                                                            <td>1</td>
                                                            <td>การประกันอัคคีภัย และ การประกันความเสี่ยงภัยทุกชนิด</td>
                                                            <td> 6</td>
                                                            <td> ยังไม่ได้อบรม</td>
                                                            <td> ผ่าน</td>
                                                            <td> ยังไม่ได้ประเมิน</td>
                                                            <td> <button type="button"
                                                                    class="btn btn-warning text-dark  "><img
                                                                        src="img/1-main/icon-p.png" alt=""></button>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>การประกันสุขภาพและประกันภัยโรคร้ายแรง</td>
                                                            <td> 6</td>
                                                            <td> ยังไม่ได้อบรม</td>
                                                            <td> ผ่าน</td>
                                                            <td> ยังไม่ได้ประเมิน</td>
                                                            <td> <button type="button"
                                                                    class="btn btn-warning text-dark  "><img
                                                                        src="img/1-main/icon-p.png" alt=""></button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7">รวมจำนวนชั่วโมงทั้งหมด 12 ชั่วโมง</td>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="toggle ">
                                        <a class="toggle-title "> <span class="numberCircle pr-3">3</span> ชื่อหลักสูตร
                                            <button type="button" class="btn btn-warning text-dark float-end"><img
                                                    src="img/1-main/icon-p.png" alt="">
                                                พิมพ์ใบประกาศนียบัตร</button></a>
                                        <div class="toggle-content ">
                                            <div class="col table-responsive my-4">
                                                <table class="table table-bordered table-questionnaire">
                                                    <thead>
                                                        <tr>
                                                            <th rowspan="2">ลำดับ</th>
                                                            <th rowspan="5" class="detail">วิชา/หลักสูตร</th>
                                                            <th rowspan="3">จำนวนชั่วโมง</th>
                                                            <th rowspan="3">สถานะการอบรม</th>
                                                            <th rowspan="3">ผลการทดสอบหลังอบรม</th>
                                                            <th rowspan="3">แบบประเมินผลการอบรม</th>
                                                            <th colspan="3">ออกใบประกาศ </th>
                                                        </tr>
                                                        <tr>
                                                            <th class="mt-2"> ภายใน วันที่ 15 ก.พ. 64</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>การประกันอัคคีภัย และ การประกันความเสี่ยงภัยทุกชนิด</td>
                                                            <td> 6</td>
                                                            <td> ยังไม่ได้อบรม</td>
                                                            <td> ผ่าน</td>
                                                            <td> ยังไม่ได้ประเมิน</td>
                                                            <td> <button type="button"
                                                                    class="btn btn-warning text-dark  "><img
                                                                        src="img/1-main/icon-p.png" alt=""></button>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>การประกันสุขภาพและประกันภัยโรคร้ายแรง</td>
                                                            <td> 6</td>
                                                            <td> ยังไม่ได้อบรม</td>
                                                            <td> ผ่าน</td>
                                                            <td> ยังไม่ได้ประเมิน</td>
                                                            <td> <button type="button"
                                                                    class="btn btn-warning text-dark  "><img
                                                                        src="img/1-main/icon-p.png" alt=""></button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7">รวมจำนวนชั่วโมงทั้งหมด 12 ชั่วโมง</td>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </section>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>