<!DOCTYPE html>
<html>

<head>
    <title>ข่าวสาร</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .dots span {
            width: 3px;
            height: 3px;
            border-radius: 50%;
            background-color: rgba(8, 8, 8, 0.5);
            display: inline-block;
            margin: 0 10px 15px;
            /* T R-L B */
        }
    </style>
</head>

<body data-plugin-page-transition>


    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li><a href="allnews.php">กิจกรรมข่าวสาร</a></li>
                                <li class="active">ชื่อกิจกรรมข่าวสาร</li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </section>

            <div class="container py-5">
                <div class="row justify-content-center">
                    <div class="col mb-4">
                        <img class="img-fluid mx-auto d-block my-3" height="auto" src="img/projects/P1.png">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8 mb-4">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                        <div class="d-flex justify-content-center dots">
                            <span></span><span></span><span></span>
                        </div>
                        <p ><span class="text-warning">1.</span> ปิดตู้เย็นให้สนิท ทำความสะอาดภายในตู้เย็น และแผ่นระบายความร้อนหลังตู้เย็นสม่ำเสมอ เพื่อให้ตู้เย็นไม่ต้องทำงานหนักและเปลืองไฟ </p>
                        <p ><span class="text-warning">2.</span> อย่าเปิดตู้เย็นบ่อย อย่านำของร้อนเข้าแช่ในตู้เย็น เพราะจะทำให้ตู้เย็นทำงานเพิ่มขึ้น กินไฟมากขึ้น </p>
                        <p ><span class="text-warning">3.</span> ตรวจสอบขอบยางประตูของตู้เย็นไม่ให้เสื่อมสภาพ เพราะจะทำให้ความเย็นรั่วออกมาได้ ทำให้สิ้นเปลืองไฟมากกว่าที่จำเป็น </p>
                        <p ><span class="text-warning">4.</span> เลือกขนาดตู้เย็นให้เหมาะสมกับขนาดครอบครัว อย่าใช้ตู้เย็นใหญ่เกินความจำเป็น เพราะกินไฟมากเกินไป และควรตั้งตู้เย็นให้ห่างจากผนังบ้าน 15 ซม. </p>
                        <p ><span class="text-warning">5.</span> ควรละลายน้ำแข็งในตู้เย็นสม่ำเสมอ การปล่อยให้น้ำแข็งจับหนาจนเกินไปจะทำให้เครื่องทำงานหนัก ทำให้กินไฟมาก </p>
                        <p ><span class="text-warning">6.</span> เลือกซื้อตู้เย็นประตูเดียว เนื่องจากตู้เย็น 2 ประตู จะกินไฟมากกว่าตู้เย็นประตูเดียวที่มีขนาดเท่ากัน เพราะต้องใช้ท่อน้ำยาทำความเย็นที่ยาวกว่า และใช้คอมเพรสเซอร์ขนาดใหญ่กว่า </p>
                        <p ><span class="text-warning">7.</span> ควรตั้งสวิตซ์ควบคุมอุณหภูมิของตู้เย็นให้เหมาะสม การตั้งที่ตัวเลขต่ำเกินไปอุณหภูมิจะเย็นน้อย ถ้าตั้งที่ตัวเลขสูงจะเย็นมาก เพื่อให้ประหยัดพลังงานควรตั้งที่เลขต่ำที่มีอุณหภูมิพอเหมาะ </p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10 my-4">
                        <img class="img-fluid mx-auto d-block mb-2" height="auto" src="img/projects/thumbnail.png">
                        <p class="text-center fst-italic">"อ้างอิงรูปภาพ"</p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8 my-4">
                        <blockquote>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus quia, illum, delectus ratione impedit commodi, accusamus dolores beatae tenetur perferendis nobis possimus. Laboriosam cupiditate mollitia esse dolorem officiis necessitatibus unde?</p>
                        </blockquote>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10 my-4">
                        <span class="border rounded-3 px-3 py-1">#TAG</span>
                        <hr class="mt-5">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 order-md-2 mb-5">
                        <div class="row justify-content-center g-4">
                            <div class="col-auto">
                                <a href="#" title="Facebook">
                                    <div class="feature-box feature-box-style-6">
                                        <div class="feature-box-icon">
                                            <img src=".\img\1-main\facebook-icon.png">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-auto">
                                <a href="#" title="Twitter">
                                    <div class="feature-box feature-box-style-6">
                                        <div class="feature-box-icon">
                                            <img src=".\img\1-main\twitter-icon.png">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-auto">
                                <a href="#" title="Line">
                                    <div class="feature-box feature-box-style-6">
                                        <div class="feature-box-icon">
                                            <img src=".\img\1-main\line-icon.png">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 order-md-1">
                        <a href="./allnews.php">
                            <button type="button" class="btn btn-outline btn-rounded btn-dark text-2 px-4"><i class="fas fa-chevron-left"></i> ย้อนกลับ</button>
                        </a>
                    </div>
                </div>
            </div>
            

        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>