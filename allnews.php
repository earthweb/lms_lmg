<!DOCTYPE html>
<html>

<head>
    <title>กิจกรรมข่าวสาร</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0 ">
                <div class="container-fluid">
                    <div class="row"><!-- 
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">กิจกรรมข่าวสาร</h1>
                        </div> -->
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li class="active">กิจกรรมข่าวสาร</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container content">
                <div class="row">
                    <div class="col-sm-6 col-lg-3 mb-4 mt-5">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/p1.png" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">ชื่อกิจกรรมข่าวสาร</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 เมษายน 2564</span></div>
                                <a href="news.php" class="text-dark  text-2 float-end" style="text-decoration: none">อ่านเพิ่มเติม <span class="text-warning "><i class="fas fa-chevron-right text-1 ms-1 "></i></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 mb-4 mt-5">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/p2.png" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">ชื่อกิจกรรมข่าวสาร</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 เมษายน 2564</span></div>
                                <a href="news.php" class="text-dark  text-2 float-end" style="text-decoration: none">อ่านเพิ่มเติม <span class="text-warning "><i class="fas fa-chevron-right text-1 ms-1 "></i></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 mb-4 mt-5">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/p3.png" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">ชื่อกิจกรรมข่าวสาร</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 เมษายน 2564</span></div>
                                <a href="news.php" class="text-dark  text-2 float-end" style="text-decoration: none">อ่านเพิ่มเติม <span class="text-warning "><i class="fas fa-chevron-right text-1 ms-1 "></i></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 mb-4 mt-5">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/p1.png" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">ชื่อกิจกรรมข่าวสาร</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 เมษายน 2564</span></div>
                                <a href="news.php" class="text-dark  text-2 float-end" style="text-decoration: none">อ่านเพิ่มเติม <span class="text-warning "><i class="fas fa-chevron-right text-1 ms-1 "></i></span></a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-lg-3 mb-4 mt-5">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/p4.png" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">ชื่อกิจกรรมข่าวสาร</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 เมษายน 2564</span></div>
                                <a href="news.php" class="text-dark  text-2 float-end" style="text-decoration: none">อ่านเพิ่มเติม <span class="text-warning "><i class="fas fa-chevron-right text-1 ms-1 "></i></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 mb-4 mt-5">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/p5.png" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">ชื่อกิจกรรมข่าวสาร</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 เมษายน 2564</span></div>
                                <a href="news.php" class="text-dark  text-2 float-end" style="text-decoration: none">อ่านเพิ่มเติม <span class="text-warning "><i class="fas fa-chevron-right text-1 ms-1 "></i></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 mb-4 mt-5">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/p6.png" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">ชื่อกิจกรรมข่าวสาร</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 เมษายน 2564</span></div>
                                <a href="news.php" class="text-dark  text-2 float-end" style="text-decoration: none">อ่านเพิ่มเติม <span class="text-warning "><i class="fas fa-chevron-right text-1 ms-1 "></i></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 mb-4 mt-5">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/p7.png" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">ชื่อกิจกรรมข่าวสาร</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 เมษายน 2564</span></div>
                                <a href="news.php" class="text-dark  text-2 float-end" style="text-decoration: none">อ่านเพิ่มเติม <span class="text-warning "><i class="fas fa-chevron-right text-1 ms-1 "></i></span></a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>

        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>