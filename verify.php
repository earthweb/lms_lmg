<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>

    <style>
        .img-upload-verify {
            width: 100%;
            /*height: 50%;*/
            background-size: cover;
            background: #FBFBFB;
            border: 2px dashed #BCBCBC;
            position: relative;
        }
        .pic-img-verify {
            position: absolute;
            /*width: 100px;
            height: 100px;*/
            width: 100%;
            background-size: cover;
            top: 65%;
            left: 64%;
            margin-top: -100px;
            margin-left: -100px;
        }
        .img-verify {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        @media (max-width: 850px) {
            .pic-img-verify {
                position: absolute;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                margin-top: 0;
                margin-left: 0;
            }
        }

        .pic-img-verify2 {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
        }

        .size-input-verify {
            position: absolute;
            width: 100%;
            height: 100%;
            opacity: 0;
            z-index: 9999;
            pointer-events: inherit;
        }

        .img-notify {
            position: relative;
        }

        .bg-verify-recommed {
            background-color: #78E1E1;
            padding: 10px;
            position: relative;
        }

        .bg-verify-recommed h6 {
            color: #006675;
        }

        .bg-verify-recommed ol li {
            color: #333333;
        }

        .arrow-verify {
            position: absolute;
            top: -19px;
            left: 22px;
        }
    </style>

</head>

<body data-plugin-page-transition>

    <div class="body">
        <div role="main" class="main">


            <div class="container-fluid center">
                <div class="row">

                    <div id="branding" class="col-md-4 d-none d-md-block"></div>
                    <div class="col col-md-4">
                        <div class="login-section ">
                            <div>
                                <img src="img/1-main/icon-p2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center mb-4">
                        แนบไฟล์รูปถ่ายของท่านคู่กับบัตรประชาชน / ใบอนุญาตนายหน้าฯ / ใบอนุญาตตัวแทน
                    </div>
                </div>

                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-4 mb-2">
                            <div>
                                <img src="./img/1-main/Verify.png" class="img-fluid" alt="">
                            </div>
                            <p style="color: #232162">
                                คำแนะนำ : การถ่ายรูปภาพคู่กับบัตรประชาชน
                            </p>
                            <div class="bg-verify-recommed">
                                <div class="arrow-verify">
                                    <img src="./img/1-main/data-notify.png" alt="">
                                </div>

                                <h6>ข้อมูลที่จำเป็นจะต้องเปิดเผย</h6>
                                <ol>
                                    <li>เลขที่บัตรประชาชน 3 ตัวท้าย (โดยปิดตัวเลขด้านหน้า)</li>
                                    <li>ชื่อและนามสกุล</li>
                                    <li>รูปภาพ</li>
                                </ol>
                            </div>
                        </div>
                        <div class="col-md-4 mb-2">
                            <div class="img-upload-verify">
                                <img src="./img/1-main/03.png" class="img-fluid" alt="">
                                <input class="size-input-verify " type="file" id="fileUpload" name="fileUpload" onchange="readURL(this)">
                                <!--<img id="imgUpload" class="img-verify" src="./img/1-main/uploadVerify.png" alt="">-->
                            </div>
                            <button type="submit" class="btn btn-main text-4 py-2 px-4 mt-4" style="float:right">ยืนยันรูปภาพ</button>
                        </div>
                    </div>
                </div>
                <!--<div class="container">

                    <div class="row justify-content-center">

                        <div class="col-md-4 col-6 mb-2">
                            <div>
                                <img src="./img/1-main/Verify.png" class="img-fluid" alt="">
                            </div>
                        </div>

                        <div class="col-md-4 col-6 mb-2">
                            <div class="img-upload-verify">
                                <img id="imgUpload" class="pic-img-verify" src="./img/1-main/uploadVerify.png" alt="">
                                <input class="size-input-verify " type="file" id="fileUpload" name="fileUpload" onchange="readURL(this)">
                            </div>
                        </div>

                    </div>

                    <div class="row justify-content-center">
                        <div class="col-md-4 col-6">

                            <p style="color: #232162">
                                คำแนะนำ : การถ่ายรูปภาพคู่กับบัตรประชาชน
                            </p>
                            <div class="bg-verify-recommed">
                                <div class="arrow-verify">
                                    <img src="./img/1-main/data-notify.png" alt="">
                                </div>

                                <h6>ข้อมูลที่จำเป็นจะต้องเปิดเผย</h6>
                                <ol>
                                    <li>เลขที่บัตรประชาชน 3 ตัวท้าย (โดยปิดตัวเลขด้านหน้า)</li>
                                    <li>ชื่อและนามสกุล</li>
                                    <li>รูปภาพ</li>
                                </ol>
                            </div>

                        </div>

                        <div class="col-md-4 col-6">
                            <button type="submit" class="btn btn-main text-4 py-2 px-4 mt-4" style="float:right">ยืนยันรูปภาพ</button>
                        </div>

                    </div>
                </div>-->
            </div>
            <!-- Footer โดนทับเลยต้องเขียนแยกครับ -->
            <footer class="border border-end-0 border-start-0 border-bottom-0 border-color-light-3 mt-5">
                <div class="bg-footer" style="z-index:1">
                    <img src="img/1-main/main-bg-3.png">
                </div>
                <div class="copyright bg-main" style="height: auto">
                    <div class="container text-center py-2">
                        <p class="mb-0 text-2 text-light"><code class="text-light">&copy;</code> 2021 by LMG Insurance Public Company Limited All right reserved.</p>
                    </div>
                </div>
            </footer>
        </div>

    </div>
    </div>

    <?php include 'include/inc-script.php'; ?>

    <!-- Script Image Preview -->
    <script>
        //Function ในการแสดงรูปภาพกตัวอย่าง
        function readURL(input) {
            var reader = new FileReader();

            reader.onload = function(e) {

                // เรียกใช้ id ชื่อ imgUpload และเพิ่ม Attribute ชื่อ src
                $('#imgUpload').attr('src', e.target.result).width(auto);

            }

            // ปรับขนาดสลับ Style
            document.getElementById('imgUpload').classList.remove("pic-img-verify");
            document.getElementById('imgUpload').classList.add("pic-img-verify2");

            reader.readAsDataURL(input.files[0]);
        }
    </script>

</body>

</html>