<!DOCTYPE html>
<html>

<head>
    <title>แบบทดสอบอบรมหลังเรียน</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        hr.style1 {
            border-top: 1px dashed rgba(8, 8, 8, 0.1);
            margin: 10px 0 22px;
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li><a href="allcourse.php">หลักสูตรทั้งหมด</a></li>
                                <li class="active">ชื่อหลักสูตร</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container py-5">
                <div class="row justify-content-center">
                    <div class="col col-lg-7">
                        <div class="card mb-4">
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <img src=".\img\1-main\check_circle_24px.png">
                                    <h6 class="text-3 mt-2">ผ่าน</h6>
                                    <h4>สรุปผลการทดสอบ</h4>
                                </div>
                                <div class="row justify-content-center my-5">
                                    <div class="col-lg-12">
                                        <div class="owl-carousel owl-theme stage-margin nav-style-1 mb-0">
                                            <?php for ($nav = 1; $nav <= 15; $nav++) { ?>
                                                <div class='exam-nav'>
                                                    <?php if($nav == 1 || $nav==2){ ?>
                                                        <div  id="nav-<?= $nav ?>" class="nav-item text-dark" style="background-color: #FFD400 !important;" onclick="selectExam(event,<?= $nav ?>);" > <?= $nav ?> </div>
                                                        <?php } else if($nav == 3) { ?>
                                                            <div  id="nav-<?= $nav ?>" class="nav-item text-dark" style="background-color: #FAA197 !important;" onclick="selectExam(event,<?= $nav ?>);" > <?= $nav ?> </div>
                                                    <?php } else { ?>
                                                        <div  id="nav-<?= $nav ?>" class="nav-item" onclick="selectExam(event,<?= $nav ?>);" > <?= $nav ?> </div>
                                                    <?php } ?>
                                                    
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <form action="#">
                                        <?php for ($exam = 1; $exam <= 15; $exam++) { ?>
                                            <div id='exam-<?= $exam ?>' class='row justify-content-center my-5 exam'>
                                            </div>
                                        <?php } ?>
                                        <div class="row justify-content-between">
                                            <div class="col-auto">
                                            </div>
                                            <div class="col-auto"></div>
                                        </div>
                                    </form>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <h6>จำนวนข้อสอบทั้งหมด</h6>
                                    </div>
                                    <div class="col text-end">
                                        <h6 class="text-warning">15 ข้อ</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>เวลาในการทำข้อสอบ</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-warning">30 นาที</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>ใช้เวลาในการสอบ</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-warning">15 นาที</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>คะแนนเต็ม</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-warning">15 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>คะแนนที่ได้</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-warning">13 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>คิดเป็นร้อยละ</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-warning">90 %</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col-8">
                                        <h6>สามารถทำแบบทดสอบได้อีก 2 ครั้ง</h6>
                                    </div>
                                    <div class="col-4 text-end">
                                        <button class="btn btn-light" style="border-radius: 5px; background-color: #F5F5F5;">
                                            <img src=".\img\1-main\icon-answer-mini.png">
                                            <a href="./answer.php" style="text-decoration: none;" >ดูเฉลย</a>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="./pretest-1.php" class="btn btn-warning text-decoration-none">
                                ทำแบบทดสอบใหม่
                            </a>
                            <a href="./coursedetail.php" class="btn btn-outline btn-warning text-decoration-none mx-2">
                                กลับหน้าหลักสูตร
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <?php include 'include/inc-footermain.php'; ?>
        </div>
        <?php include 'include/inc-script.php'; ?>

        <script>
            document.getElementById("exam-1").style.display = "block";
            document.getElementById("nav-1").className += " nav-active";
            var currentExam = 1;

            var owl = $('.owl-carousel');
            owl.owlCarousel({
                margin: 0,
                loop: false,
                nav: true,
                dots: false,
                autoHeight: true,
                stagePadding: 40,
                responsive: {
                    0: {
                        items: 3,
                        slideBy: 3,
                    },
                    500: {
                        items: 5,
                        slideBy: 5,
                    },
                    768: {
                        items: 10,
                        slideBy: 10,
                    }
                }
            });

            function selectChoice(evt, parentIdNum) {
                var i, choice, parentId, navId, nav;
                parentId = "choiceGroup-" + parentIdNum.toString();
                choice = document.getElementById(parentId).children;
                navId = "nav-" + parentIdNum.toString();
                nav = document.getElementById(navId)

                for (i = 0; i < choice.length; i++) {
                    choice[i].className = choice[i].className.replace(" choice-check", "");
                }
                evt.currentTarget.className += " choice-check";

                nav.className.replace(" nav-check", "");
                nav.className += " nav-check";
            }

        </script>
</body>

</html>