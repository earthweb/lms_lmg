<!DOCTYPE html>
<html>

<head>
    <title>เอกสารดาวน์โหลด</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li class="active">เอกสารดาวน์โหลด</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="content position-relative">

                <!-- <div class="bg-page">
                    <img src="img/1-main/main-bg-1.png">
                </div> -->

                <div class="container">
                    <div class="row justify-content-between align-items-end mb-3">
                        <div class="col-7 col-md-8 col-lg-9">
                            <h4 class="topic mb-0"> เอกสารล่าสุด</h4>
                        </div>
                        <div class="col-5 col-md-4 col-lg-3">
                            <input class="form-control text-3" type="text" style="width: 100%;" placeholder="พิมพ์คำค้นหา">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col table-responsive">
                            <table class="table table-document">
                                <thead>
                                    <tr>
                                        <td>ลำดับ</td>
                                        <td>รายการ</td>
                                        <td>วันที่ประกาศ</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <td>11/05/2564</td>
                                        <td>
                                            <button class="btn btn-download" type="button">ดาวน์โหลด</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <td>9/05/2564</td>
                                        <td>
                                            <button class="btn btn-download" type="button">ดาวน์โหลด</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <td>8/05/2564</td>
                                        <td>
                                            <button class="btn btn-download" type="button">ดาวน์โหลด</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <td>7/05/2564</td>
                                        <td>
                                            <button class="btn btn-download" type="button">ดาวน์โหลด</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <td>5/05/2564</td>
                                        <td>
                                            <button class="btn btn-download" type="button">ดาวน์โหลด</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <td>5/05/2564</td>
                                        <td>
                                            <button class="btn btn-download" type="button">ดาวน์โหลด</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            

        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>