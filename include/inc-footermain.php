<footer class="border border-end-0 border-start-0 border-bottom-0 border-color-light-3 mt-0"> 
    <div class="bg-footer">
        <img src="img/1-main/main-bg-3.png">
    </div>
    <div class="copyright bg-main" style="height: auto">
        <div class="container text-center py-2">
        <p class="mb-0 text-2 text-light"><code class="text-light">&copy;</code> 2021 by LMG Insurance Public Company Limited All right reserved.</p>
        </div> 
    </div>  
</footer> 