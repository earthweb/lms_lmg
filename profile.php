<!DOCTYPE html>
<html>

<head>
    <title>ข้อมูลส่วนตัว</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li class="active">ข้อมูลส่วนตัว</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="row g-0 position-relative sidebar-bg-hide">

                <div class="bg-page">
                    <img src="img/1-main/main-bg-1.png">
                </div>

                <div class="sidebar col-md-3 col-lg-2">
                    <div class="container p-3 border">
                        <div class="container-fluid ">
                            <p class="my-0">ข้อมูลส่วนตัว</p>
                            <hr class="my-2">
                            <p class="my-0"><a href="./status-user.php">สถานะการเรียน</a></p>
                        </div>
                    </div>
                </div>

                <div class="content col col-md-9 col-lg-10 ">
                    <div class="card mb-4">
                        <h5 class="card-header bg-warning p-3">ข้อมูลส่วนตัว</h5>
                        <div class="card-body border-input">
                            <div class="form-row mb-3">
                                <div class="col-lg-10">

                                    <div class="img-upload-profile">
                                        <div>
                                            <img id="imgUpload" class="pic-img-upload" src="./img/1-main/pic-upload-image.png" alt="">
                                            <input class="size-input" type="file" id="fileUpload" name="fileUpload" onchange="readURL(this)">
                                        </div>
                                    </div>
                                </div>
                                <label class="col-lg-2 col-sm-5 col-12">
                                    <h5>คำนำหน้า</h5>
                                    <select class="form-control">
                                        <option selected="">คำนำหน้า</option>
                                        <option value="1">นาย</option>
                                        <option value="2">นาง</option>
                                        <option value="3">นางสาว</option>
                                    </select>
                                </label>

                            </div>

                            <div class="form-row mb-3">
                                <label class="col-lg-4 col-12">
                                    <h5 class="card-title">ชื่อ</h5>
                                    <input class="floating-input form-control" type="text" placeholder=" ">
                                </label>

                                <label class="col-lg-4 col-12">
                                    <h5 class="card-title">นามสกุล</h5>
                                    <input class="floating-input form-control" type="text" placeholder=" ">
                                </label>
                            </div>

                            <div class="col-lg-2"></div>
                            <div class="form-row mb-5">
                                <label class="col-lg-4 col-12">
                                    <h5 class="card-title">หมายเลขบัตรประชาชน</h5>
                                    <input class="floating-input form-control" type="text" placeholder=" ">
                                </label>

                                <label class="col-lg-4 col-12">
                                    <h5 class="card-title">รหัสตัวแทน</h5>
                                    <input class="floating-input form-control" type="text" placeholder=" ">
                                </label>

                                <label class="col-lg-3 col-12">
                                    <h5>Email</h5>
                                    <input class="floating-input form-control" type="text" placeholder=" ">
                                </label>
                            </div>
                            <hr class="mb-3">
                            <div class="form-row ">
                                <label class="col-lg-4 col-12">
                                    <h5 class="card-title">ชื่อผู้ใช้</h5>
                                    <input class="floating-input form-control" type="text" placeholder=" ">
                                </label>

                                <label class="col-lg-4 col-12">
                                    <h5 class="card-title">รหัสผ่าน</h5>
                                    <input class="floating-input form-control" type="password" placeholder=" ">
                                </label>

                                <label class="col-lg-3 col-12">
                                    <h5 class="card-title">ยืนยันรหัสผ่าน</h5>
                                    <input class="floating-input form-control" type="password" placeholder=" ">
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="" style="text-align:center;">
                        <a class="btn btn-warning text-dark btn-submit-profile " href="#" role="button">ยืนยัน</a>
                    </div>
                </div>


            </div>
        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>

    <!-- Script Image Preview -->
    <script>
        //Function ในการแสดงรูปภาพกตัวอย่าง
        function readURL(input) {
            var reader = new FileReader();

            reader.onload = function(e) {

                // เรียกใช้ id ชื่อ imgUpload และเพิ่ม Attribute ชื่อ src
                $('#imgUpload').attr('src', e.target.result).width(auto)
            }

            reader.readAsDataURL(input.files[0]);
        }
    </script>


</body>

</html>