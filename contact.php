<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .a {
            display: block;
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li class="active">ติดต่อเรา</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container content">
                <div class="row mb-4 mt-3 ">

                    <div class="col-lg-6 px-0  ">

                        <h3 class="title-panel"><span>แผนที่สำนักงาน</span></h3>
                        <div class="google-map mt-0 mb-3" style="height: 350px;">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.7129950907997!2d100.56079871465401!3d13.735818190357822!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2bd35f50a61d7%3A0x313043be954b229f!2z4Lia4Lij4Li04Lip4Lix4LiXIOC5geC4reC4peC5gOC4reC5h-C4oeC4iOC4tSDguJvguKPguLDguIHguLHguJnguKDguLHguKIg4LiI4Liz4LiB4Lix4LiUICjguKHguKvguLLguIrguJkp!5e0!3m2!1sth!2sth!4v1625482293986!5m2!1sth!2sth" width="100%" height="350" allowfullscreen="" loading="lazy"></iframe>
                        </div>

                    </div>

                    <div class="col-lg-6 px-0 card-right-contact">

                        <div class="card-body row">
                            <h3 class="title-panel"><span>ข้อมูลที่อยู่</span></h3>
                            <div class="col-auto">
                                <img src=".\img\1-main\pin-icon.png">
                            </div>
                            <div class="col">
                                <h6 class="mb-1 text-4 ">บริษัท แอลเอ็มจี ประกันภัย จำกัด (มหาชน)</h6>
                                <p>ชั้น 14,15, 17 และ 19 อาคารจัสมินซิตี้ เลขที่ 2 ซอยสุขุมวิท 23 ถนนสุขุมวิท แขวงคลองเตยเหนือ เขตวัฒนา กรุงเทพฯ 10110</p>
                            </div>

                            <h3 class="title-panel"><span>ติดต่อเจ้าหน้าที่</span></h3>
                            <h3><small> แก้ปัญหาที่ใช้งาน<a href="#" class="text-danger">"คลิกที่นี่"</a></small></h3>
                            
                            <div class="col-auto">
                                <img src=".\img\1-main\tel-icon.png">
                            </div>
                            <div class="col-lg-4 col-8">
                                <h6 class="mb-1 text-4 ">โทรศัพท์</h6>
                                <p>0-2661-6000</p>
                            </div>
                            <div class="col-auto">
                                <img src=".\img\1-main\tel2.png">
                            </div>
                            <div class="col-lg-4 col-5">
                                <h6 class="mb-1 text-4 ">โทรสาร</h6>
                                <p>0-2665-2728</p>
                            </div>
                            
                        </>
                    </div>
                </div>
            </div>


        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>