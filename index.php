<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main text-color-h3">

            <section class="menu-index mt-1 mb-1">

                <div class="container-fulid">
                    <div class="row">

                        <div class="col-md-1"></div>

                        <a href="#" class="col-md-2 col-sm-12 col-12 mt-3 px-md-1 px-5">
                            <div class="bg-box">
                                <img src="./img/1-main/students1.png" alt="">
                            </div>
                        </a>

                        <a href="library.php" class="col-md-2 col-sm-12 col-12 mt-3 px-md-1 px-5">
                            <div class="bg-box">
                                <img src="./img/1-main/elibrary.png" alt="">
                            </div>
                        </a>
                        <a href="document.php" class="col-md-2 col-sm-12 col-12 mt-3 px-md-1 px-5">
                            <div class="bg-box">
                                <img src="./img/1-main/downloadpaper.png" alt="">
                            </div>
                        </a>
                        <a href="plan.php" class="col-md-2 col-sm-12 col-12 mt-3 px-md-1 px-5">
                            <div class="bg-box">
                                <img src="./img/1-main/plane.png" alt="">
                            </div>
                        </a>
                        <a href="announcement.php" class="col-md-2 col-sm-12 col-12 mt-3 px-md-1 px-5">
                            <div class="bg-box">
                                <img src="./img/1-main/table-notify.png" alt="">
                            </div>
                        </a>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </section>

            <section class="banner-home">
                <div class="container-fulid">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <img src="./img/1-main/banner-img.jpg" width="100%" alt="">
                            </div>
                            <div class="help_mascot" data-bs-toggle="modal" data-bs-target="#largeModal1">
                                <img class="img_mascot" src="img/1-main/mascot-main.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="bg-course">
                    <!-- <img src="img/1-main/main-bg-1.png"> -->
                </div>
                <div class="container pb-5">
                    <div class="row mt-5">
                        <div class="col-12">
                            <h3 class="float-start"><span style="border-bottom: 3px solid #FECD22;">หลักสูตรของเรา</span></h3>
                            <a class="float-end" href="allcourse.php"><button class="btn btn-outline-course">หลักสูตรทั้งหมด</button></a>
                        </div>
                        <!--<div class="col-md-6 col-6">
                            <h3><span style="border-bottom: 3px solid #FECD22;">หลักสูตรของเรา</span></h3>      
                        </div>
                        <a class="col-md-6 col-6" href="allcourse.php"><button class="btn btn-outline-course float-end">หลักสูตรทั้งหมด</button></a>-->

                        <div class="col-md-3 col-sm-6">
                            <div class="py-4 px-1">
                                <div class="card card-course">
                                    <div>
                                        <a href="coursedetail.php">
                                            <img class="card-img-top" src="img/1-main/pic-course1.png">
                                            <img src="./img/1-main/logo-card.png" class="logo-card" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title mb-2 text-4 text-main ">ชื่อหลักสูตร</h6>
                                        <hr class="mb-2">
                                        <span class="card-text "><i style="color: #FECD22" class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                        <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="py-4 px-1">
                                <div class="card card-course">
                                    <div>
                                        <a href="coursedetail.php">
                                            <img class="card-img-top" src="img/1-main/pic-course2.png">
                                            <img src="./img/1-main/logo-card.png" class="logo-card" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title mb-2 text-4 text-main ">ชื่อหลักสูตร</h6>
                                        <hr class="mb-2">
                                        <span class="card-text "><i style="color: #FECD22" class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                        <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="py-4 px-1">
                                <div class="card card-course">
                                    <div>
                                        <a href="coursedetail.php">
                                            <img class="card-img-top" src="img/1-main/pic-course3.png">
                                            <img src="./img/1-main/logo-card.png" class="logo-card" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title mb-2 text-4 text-main ">ชื่อหลักสูตร</h6>
                                        <hr class="mb-2">
                                        <span class="card-text "><i style="color: #FECD22" class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                        <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="py-4 px-1">
                                <div class="card card-course">
                                    <div>
                                        <a href="coursedetail.php">
                                            <img class="card-img-top" src="img/1-main/pic-course4.png">
                                            <img src="./img/1-main/logo-card.png" class="logo-card" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title mb-2 text-4 text-main ">ชื่อหลักสูตร</h6>
                                        <hr class="mb-2">
                                        <span class="card-text "><i style="color: #FECD22" class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                        <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="py-4 px-1">
                                <div class="card card-course">
                                    <div>
                                        <a href="coursedetail.php">
                                            <img class="card-img-top" src="img/1-main/pic-course5.png">
                                            <img src="./img/1-main/logo-card.png" class="logo-card" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title mb-2 text-4 text-main ">ชื่อหลักสูตร</h6>
                                        <hr class="mb-2">
                                        <span class="card-text "><i style="color: #FECD22" class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                        <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="py-4 px-1">
                                <div class="card card-course">
                                    <div>
                                        <a href="coursedetail.php">
                                            <img class="card-img-top" src="img/1-main/pic-course6.png">
                                            <img src="./img/1-main/logo-card.png" class="logo-card" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title mb-2 text-4 text-main ">ชื่อหลักสูตร</h6>
                                        <hr class="mb-2">
                                        <span class="card-text "><i style="color: #FECD22" class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                        <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-6">
                            <div class="py-4 px-1">
                                <div class="card card-course">
                                    <div>
                                        <a href="coursedetail.php">
                                            <img class="card-img-top" src="img/1-main/pic-course7.png">
                                            <img src="./img/1-main/logo-card.png" class="logo-card" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title mb-2 text-4 text-main ">ชื่อหลักสูตร</h6>
                                        <hr class="mb-2">
                                        <span class="card-text "><i style="color: #FECD22" class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                        <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="py-4 px-1">
                                <div class="card card-course">
                                    <div>
                                        <a href="coursedetail.php">
                                            <img class="card-img-top" src="img/1-main/pic-course8.png">
                                            <img src="./img/1-main/logo-card.png" class="logo-card" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title mb-2 text-4 text-main ">ชื่อหลักสูตร</h6>
                                        <hr class="mb-2">
                                        <span class="card-text "><i style="color: #FECD22" class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                        <a href="coursedetail.php">
                                            <span class="float-end text-dark">ดูรายละเอียด <img src="./img/1-main/arrow-left.svg" alt=""></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="activity-index">
                <div class="container" >
                    <div class="row ">
                        <div class="col-lg-8 mt-2 ">
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <h3>กิจกรรมข่าวสาร</h3>
                                </div>
                                <a class="col-md-6 col-6 px-3" href="allnews.php"><button class="btn btn-outline-course float-end">ดูทั้งหมด</button></a>
                            </div>

                            <div>
                                <div class="row">

                                    <div class="col-lg-4 mb-4">
                                        <article class="post">
                                            <div class="post-image">
                                                <a href="news.php">
                                                    <img src="img/1-main/pic-new1.png" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-1 mb-2" alt="" />
                                                </a>
                                            </div>
                                            <div>
                                                <div class="blog-detail">
                                                    <p class="mb-1 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam nisi ultricies nisi luctus, sed fermentum. </p>

                                                    <div><i style="color: #78E1E1" class="icon-calendar"></i> 10 เมษายน 2564
                                                        <a href="news.php">
                                                            <span class="float-end text-dark">อ่านเพิ่มเติม <img src="./img/1-main/arrow-left-new.svg" alt=""></span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </article>
                                    </div>

                                    <div class="col-lg-4 mb-4">
                                        <article class="post">
                                            <div class="post-image">
                                                <a href="news.php">
                                                    <img src="img/1-main/pic-new2.png" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-1 mb-2" alt="" />
                                                </a>
                                            </div>
                                            <div>
                                                <div class="blog-detail">
                                                    <p class="mb-1 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam nisi ultricies nisi luctus, sed fermentum. </p>

                                                    <div><i style="color: #78E1E1" class="icon-calendar"></i> 10 เมษายน 2564
                                                        <a href="news.php">
                                                            <span class="float-end text-dark">อ่านเพิ่มเติม <img src="./img/1-main/arrow-left-new.svg" alt=""></span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </article>
                                    </div>

                                    <div class="col-lg-4 mb-4">
                                        <article class="post">
                                            <div class="post-image">
                                                <a href="news.php">
                                                    <img src="img/1-main/pic-new3.png" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-1 mb-2" alt="" />
                                                </a>
                                            </div>
                                            <div>
                                                <div class="blog-detail">
                                                    <p class="mb-1 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam nisi ultricies nisi luctus, sed fermentum. </p>

                                                    <div><i style="color: #78E1E1" class="icon-calendar"></i> 10 เมษายน 2564
                                                        <a style="color: #1A1444 !important" href="news.php">
                                                            <span class="float-end">อ่านเพิ่มเติม <img src="./img/1-main/arrow-left-new.svg" alt=""></span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </article>
                                    </div>


                                </div>
                            </div>

                        </div><!-- style="background-image: url('img/1-main/main-bg-2.png');" -->
                        <div class="col col-lg-4 col-12 mt-2">
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <h3>วีดีโอแนะนำ</h3>
                                </div>
                                <a class="col-md-6 col-6 px-3" href="video.php"><button class="btn btn-outline-course float-end">ดูทั้งหมด</button></a>
                            </div>

                            <div>
                                <article class="post">
                                    <div class="post-image">
                                        <a href="#">
                                            <video class="w-100" controls>
                                                <source src="video/deves-video.mp4" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>
                                            <!-- <img src="img/1-main/thumbnail.png" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0 mb-2" alt="" /> -->
                                        </a>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="modal fade" id="largeModal1" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header  bg-warning text-dark">
                        <h4 class="modal-title" id="largeModalLabel"><img src="./img/1-main/icon-problem.png" alt=""> แจ้งปัญหาการใช้งาน</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body border-input">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="text-dark" for="">User</label>
                                <input class="form form-control" type="text" value="LMG1400300057756" disabled>
                            </div>
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <label class="text-dark" for="">ชื่อ</label>
                                <input class="form form-control" type="text" placeholder="นายเอลแอ็มจี">
                            </div>
                            <div class="col-md-6">
                                <label class="text-dark" for="">นามสกุล</label>
                                <input class="form form-control" type="text" placeholder="ประกันภัย">
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark" for="">เบอร์โทรศัพท์</label>
                                <input class="form form-control" type="text" placeholder="เบอร์โทรศัพท์">
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark" for="">LINE ID</label>
                                <input class="form form-control" type="text" placeholder="LINE ID">
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark" for="">อีเมล์</label>
                                <input class="form form-control" type="text" placeholder="อีเมล์">
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-dark" for="exampleFormControlSelect1">ประเภทปัญหา</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>ไม่ระบุประเภทปัญหา</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-dark" for="exampleFormControlSelect1">หลักสูตร</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>ไม่ระบุประเภทปัญหา</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label class="text-dark" for="">ข้อความ</label>
                                <input type="text" class="form-control">
                            </div>

                            <div class="col-md-5 upload-image">

                                <label for="">อัพโหลดรูปภาพ</label>
                                <div class="input-group mb-3 mt-2">
                                    <input type="file" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2">
                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">

                        <!-- <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button> -->
                        <button type="button" class="btn btn-modal-footer">ยืนยัน</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Footer ทับ Background -->
        <div class="margin-footer-home">
            <footer class="border border-end-0 border-start-0 border-bottom-0 border-color-light-3 mt-0">
                <div class="bg-footer">
                    <img src="img/1-main/main-bg-3.png" style="z-index: 1;">
                </div>
                <div class="copyright bg-main" style="height: auto">
                    <div class="container text-center py-2">
                        <p class="mb-0 text-2 text-light"><code class="text-light">&copy;</code> 2021 by LMG Insurance Public Company Limited All right reserved.</p>
                    </div>
                </div>
            </footer>
        </div>

    </div>

    <?php include 'include/inc-script.php'; ?>



</body>

</html>