<!DOCTYPE html>
<html>

<head>
    <title>แบบประเมินหลักสูตร</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .table-questionnaire {
            min-width: 700px;
            text-align: center;
        }

        .table-questionnaire thead tr th {
            vertical-align: middle;
        }

        .table-questionnaire tbody tr td:nth-child(2) {
            text-align: left;
            width: 300px;
        }

        .table-questionnaire td,
        .table-questionnaire th {
            width: 75px;
        }

        #suggestion {
            width: 100%;
            height: 100px;
            padding: 10px;
        }
    </style>
    <link href="./vendor/icheck/skins/square/yellow.css" rel="stylesheet">
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li class="active">แบบประเมินหลักสูตร</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container content">
                <div class="row">
                    <h4 class="mb-2 mt-2">>> แบบประเมินความพึงพอใจหลักสูตร</h4>
                     <h5 class="mb-0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วิทยากร : คุณชัยรัตน์ เกิดเพิ่มพูล</h5>
                    <form>
                        <div class="col table-responsive my-4">
                            <table class="table table-bordered table-questionnaire" >
                                <thead class="questionnaire-title">
                                    <tr>
                                        <th rowspan="2">ลำดับ</th>
                                        <th rowspan="2" class="detail">หัวข้อประเมิน</th>
                                        <th colspan="5">ระดับคะแนน</th>
                                    </tr>
                                    <tr>
                                        <th>5<br>ดีมาก</th>
                                        <th>4<br>ดี</th>
                                        <th>3<br>ปานกลาง</th>
                                        <th>2<br>พอใช้</th>
                                        <th>1<br>ปรับปรุง</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <tr >
                                        <td colspan="7" class="textleft">1. ความรู้ความเข้าใจเนื้อหาหลักสูตร</td>
                                    </tr>
                                    <tr>
                                        <td>1.1</td>
                                        <td>เนื้อหาครบถ้วนครอบคลุมตามวัตถุประสงค์ของหลักสูตร</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="1" value="5"></td>
                                        <td><input type="radio" name="1" value="4"></td>
                                        <td><input type="radio" name="1" value="3"></td>
                                        <td><input type="radio" name="1" value="2"></td>
                                        <td><input type="radio" name="1" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                    <tr>
                                        <td>1.2</td>
                                        <td>ได้รับความรู้ความเข้าใจเนื้อหาของหลักสูตรในระดับใด</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="2" value="5"></td>
                                        <td><input type="radio" name="2" value="4"></td>
                                        <td><input type="radio" name="2" value="3"></td>
                                        <td><input type="radio" name="2" value="2"></td>
                                        <td><input type="radio" name="2" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                    <tr>
                                        <td>1.3</td>
                                        <td>สามารถนพเนื้อหาที่เรียนไปใช้ประโยชน์ในการปฏิบัติงานได้ในระดับใด</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="3" value="5"></td>
                                        <td><input type="radio" name="3" value="4"></td>
                                        <td><input type="radio" name="3" value="3"></td>
                                        <td><input type="radio" name="3" value="2"></td>
                                        <td><input type="radio" name="3" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col card mb-3">
                            <div class="card-body">
                                <h6>ความคิดเห็นเพิ่มเติม หรือข้อแนะนำ</h6>
                                <textarea class="questionnaire-title " id="suggestion" placeholder="คำตอบของคุณ"></textarea>
                            </div>
                        </div>
                        <div class="text-center mb-5" style="z-index: 9999">
                            <a href="coursedetail.php" class="btn btn-main text-decoration-none " >
                                  ส่งแบบประเมิน
                            </a>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>
    <script src="./vendor/icheck/icheck.js"></script>
    <script>
        $(document).ready(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square',
                radioClass: 'iradio_square'
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-yellow',
                radioClass: 'iradio_square-yellow'
            });
        });
    </script>

</body>

</html>