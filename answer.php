<!DOCTYPE html>
<html>

<head>
    <title>แบบทดสอบอบรมหลังเรียน</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        hr.style1 {
            border-top: 1px dashed rgba(8, 8, 8, 0.1);
            margin: 10px 0 22px;
        }
        .box {
            display: flex;
        }
        .a {
            flex: 0.5 1 auto;
        }
        .b {
            flex: 2;
        }
        .c {
            flex: 1 1 auto;
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./">หน้าแรก</a></li>
                                <li><a href="allcourse.php">หลักสูตรทั้งหมด</a></li>
                                <li class="active">ชื่อหลักสูตร</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container py-5">
                <div class="row justify-content-center">
                    <div class="col col-lg-7">
                        <div class="card mb-4">
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <img src=".\img\1-main\icon-answer.png">                                 
                                    <h4>เฉลยแบบทดสอบ</h4>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <h6>คะแนนรวมผลการทดสอบ</h6>
                                    </div>
                                    <div class="col text-end">
                                        <h6 class="text-warning">13 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center" >
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class=" b">
                                        <h6> คำถามข้อที่ 1</h6>
                                        <h6> 1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 2</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-times-circle" style="color: red;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 3</h6>
                                        <h6>1+1 = 5</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">0 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 4</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 5</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 6</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 7</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-times-circle" style="color: red;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 8</h6>
                                        <h6>1+1 = 5</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">0 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 9</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 10</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 11</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 12</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-times-circle" style="color: red;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 13</h6>
                                        <h6>1+1 = 5</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">0 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 14</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="box" style="align-items: center">
                                    <div class="a">
                                        <i class="fas fa-check-circle" style="color: green;"></i>
                                    </div>
                                    <div class="b">
                                        <h6>คำถามข้อที่ 15</h6>
                                        <h6>1+1 = 2</h6>
                                    </div>
                                    <div class="c text-end">
                                        <h6 class="text-warning">1 คะแนน</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="./pretest-3.php" class="btn btn-outline btn-warning text-decoration-none mx-2">
                                ย้อนกลับ
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <?php include 'include/inc-footermain.php'; ?>
        </div>
        <?php include 'include/inc-script.php'; ?>

</body>

</html>